# PluginUpdater

A Bukkit server plugin for Minecraft that allows for easy plugin updating and installation.

This was made to suit the needs of the [Diamond Mine](http://diamondmine.net) server.

**[License](https://diamondmine.net/plugins/license)** -- **[Issues](https://mcftmedia.atlassian.net/issues/?jql=project=UPDATER)** --  **[Builds](https://mcftmedia.atlassian.net/builds/browse/UPDATER-UPDATER)**
-- **[Download](http://diamondmine.net/plugins/download/PluginUpdater)** -- **[Bukkit Dev](http://dev.bukkit.org/server-mods/pluginupdater/)**