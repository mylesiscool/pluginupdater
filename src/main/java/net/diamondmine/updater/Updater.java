package net.diamondmine.updater;

import java.io.File;

/**
 * Main framework interface.
 * 
 * @author Jon la Cour
 * @see UpdaterInstance
 */
public interface Updater {

    /**
     * Get the directory used to store configuration.
     * 
     * @return directory used to store configuration
     */
    File getDataDirectory();

    /**
     * Get the version number.
     * 
     * @return version number
     */
    String getVersionNumber();

}
