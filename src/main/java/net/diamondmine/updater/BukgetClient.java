package net.diamondmine.updater;

import java.io.IOException;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;

public class BukgetClient {

    public HttpClient client;
    public GetMethod get = null;
    public PostMethod post = null;

    public int status = HttpStatus.SC_PROCESSING;
    public byte[] response = {};

    private static String hostname = "api.bukget.org";
    private static String version = "3";
    private static String useragent = "PluginUpdater";

    public void get(String path) {
        client = new HttpClient();
        get = new GetMethod("http://" + hostname + "/" + version + "/" + path);

        setCommonParams();

        try {
            status = client.executeMethod(get);
            response = get.getResponseBody();
        } catch (HttpException e) {
            System.err.println("Fatal protocol violation: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("Fatal transport error: " + e.getMessage());
            e.printStackTrace();
        } finally {
            get.releaseConnection();
        }
    }

    public void post(String path, NameValuePair[] data) {
        client = new HttpClient();
        post = new PostMethod("http://" + hostname + "/" + version + "/" + path);
        post.addParameters(data);

        setCommonParams();

        try {
            status = client.executeMethod(post);
            response = post.getResponseBody();
        } catch (HttpException e) {
            System.err.println("Fatal protocol violation: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("Fatal transport error: " + e.getMessage());
            e.printStackTrace();
        } finally {
            post.releaseConnection();
        }
    }

    public void setMethod(String name, Object value) {
        if (get != null) {
            get.getParams().setParameter(name, value);
        } else if (post != null) {
            post.getParams().setParameter(name, value);
        }
    }

    private void setCommonParams() {
        String version = UpdaterInstance.getInstance().getVersionNumber();

        setMethod(HttpMethodParams.SO_TIMEOUT, 1000);
        setMethod(HttpMethodParams.HTTP_CONTENT_CHARSET, "UTF-8");
        setMethod(HttpMethodParams.USER_AGENT, useragent + "/" + version);
        setMethod(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(3, false));
    }
}
