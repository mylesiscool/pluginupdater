package net.diamondmine.updater.types.resource;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class Zip extends Resource {
    /**
     * Zip resource.
     * 
     * @param url
     *        URL of resource.
     * @param type
     *        Type of resource (jar, zip, or rar).
     * @param name
     *        Name of resource.
     * @throws Exception
     *         Invalid file.
     */
    public Zip(final String pluginName, final String fileName, final URL url) throws Exception {
        super(pluginName, fileName, url);
    }

    @Override
    protected final void process() throws Exception {
        int buffer = 2048;
        String path = "plugins" + File.separator + fileName;
        File file = new File(path);
        ZipFile zip = new ZipFile(file);

        try {
            Enumeration<? extends ZipEntry> zipFileEntries = zip.entries();

            while (zipFileEntries.hasMoreElements()) {
                ZipEntry entry = zipFileEntries.nextElement();
                String currentEntry = entry.getName();
                File destFile = new File("plugins", currentEntry);
                File destParent = destFile.getParentFile();
                destParent.mkdirs();

                if (!entry.isDirectory() && entry.getName().toLowerCase().endsWith(".jar")) {
                    BufferedInputStream is = new BufferedInputStream(zip.getInputStream(entry));
                    int currentByte;
                    byte[] data = new byte[buffer];

                    FileOutputStream fos = new FileOutputStream(destFile);
                    BufferedOutputStream dest = new BufferedOutputStream(fos, buffer);

                    while ((currentByte = is.read(data, 0, buffer)) != -1) {
                        dest.write(data, 0, currentByte);
                    }
                    dest.flush();
                    dest.close();
                    is.close();
                }
            }
        } finally {
            zip.close();
            file.delete();
        }
    }
}
