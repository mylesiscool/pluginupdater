package net.diamondmine.updater.types.resource;

import java.net.URL;

import net.diamondmine.updater.util.PluginDownloader;

public abstract class Resource {
    public String pluginName;
    public String fileName;
    public URL url;

    public Resource(final String pluginName, final String fileName, final URL url) {
        this.pluginName = pluginName;
        this.fileName = fileName;
        this.url = url;
    }

    public final boolean download() throws Exception {
        PluginDownloader.download(url, fileName);

        process();

        return true;
    }

    protected abstract void process() throws Exception;
}