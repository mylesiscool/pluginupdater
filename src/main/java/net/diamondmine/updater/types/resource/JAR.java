package net.diamondmine.updater.types.resource;

import java.net.URL;

public class JAR extends Resource {
    /**
     * JAR resource.
     * 
     * @param url
     *        URL of resource.
     * @param type
     *        Type of resource (jar, zip, or rar).
     * @param name
     *        Name of resource.
     */
    public JAR(final String pluginName, final String fileName, final URL url) throws Exception {
        super(pluginName, fileName, url);
    }

    @Override
    protected final void process() {
        return;
    }
}