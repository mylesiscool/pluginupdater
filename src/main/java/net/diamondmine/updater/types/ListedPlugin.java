package net.diamondmine.updater.types;

import java.util.ArrayList;
import java.util.List;

public class ListedPlugin {
    private List<String> authors;
    private List<String> categories;
    private String dboPage;
    private String description;
    private String logo;
    private String logoFull;
    private String plugin_name;
    private String server;
    private String slug;
    private String status;
    private List<ListedVersion> versions;
    private String webpage;

    public void addVersion(ListedVersion version) {
        if (this.versions == null) {
            this.versions = new ArrayList<ListedVersion>();
        }

        if (version != null) {
            this.versions.add(version);
        }
    }

    public List<String> getAuthors() {
        return authors;
    }

    public List<String> getCategories() {
        return categories;
    }

    public String getDboPage() {
        return dboPage;
    }

    public String getDescription() {
        return description;
    }

    public String getLogo() {
        return logo;
    }

    public String getLogoFull() {
        return logoFull;
    }

    public String getPluginName() {
        return plugin_name;
    }

    public String getServer() {
        return server;
    }

    public String getSlug() {
        return slug;
    }

    public String getStatus() {
        return this.status;
    }

    public List<ListedVersion> getVersions() {
        return versions;
    }

    public String getWebpage() {
        return webpage;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public void setDboPage(String dboPage) {
        this.dboPage = dboPage;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public void setLogoFull(String logoFull) {
        this.logoFull = logoFull;
    }

    public void setPluginName(String plugin_name) {
        this.plugin_name = plugin_name;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setVersions(List<ListedVersion> versions) {
        this.versions = versions;
    }

    public void setWebpage(String webpage) {
        this.webpage = webpage;
    }
}