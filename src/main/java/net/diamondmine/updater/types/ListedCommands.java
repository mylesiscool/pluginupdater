package net.diamondmine.updater.types;

import java.util.List;

public class ListedCommands {
    private List<String> alias;
    private String command;
    private String permission;
    private String permissionMessage;
    private String usage;

    public List<String> getAliases() {
        return alias;
    }

    public String getCommand() {
        return command;
    }

    public String getPermission() {
        return permission;
    }

    public String getPermissionMessage() {
        return permissionMessage;
    }

    public String getUsage() {
        return usage;
    }
}