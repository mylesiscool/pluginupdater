package net.diamondmine.updater.types;

public class ListedPermissions {
    private String default_;
    private String role;

    /**
     * @return the default_
     */
    public String getDefault() {
        return default_;
    }

    /**
     * @return the role
     */
    public String getRole() {
        return role;
    }

    /**
     * @param default_
     *        the default_ to set
     */
    public void setDefault(String default_) {
        this.default_ = default_;
    }

    /**
     * @param role
     *        the role to set
     */
    public void setRole(String role) {
        this.role = role;
    }
}
