package net.diamondmine.updater.types;

import java.util.List;

public class ListedVersion {
    private String changelog;
    private List<ListedCommands> commands;
    private long date;
    private String download;
    private String filename;
    private List<String> game_versions;
    private List<String> hard_dependencies;
    private String link;
    private String md5;
    private List<ListedPermissions> permissions;
    private String slug;
    private List<String> soft_dependencies;
    private String status;
    private String type;
    private String version;

    public String getChangelog() {
        return changelog;
    }

    public List<ListedCommands> getCommands() {
        return commands;
    }

    public long getDate() {
        return date;
    }

    public String getDownload() {
        return download;
    }

    public String getFilename() {
        return filename;
    }

    public List<String> getGameVersions() {
        return game_versions;
    }

    public List<String> getHardDependencies() {
        return hard_dependencies;
    }

    public String getLink() {
        return link;
    }

    public String getMD5() {
        return md5;
    }

    public List<ListedPermissions> getPermissions() {
        return permissions;
    }

    public String getSlug() {
        return slug;
    }

    public List<String> getSoftDependencies() {
        return soft_dependencies;
    }

    public String getStatus() {
        return status;
    }

    public String getType() {
        return type;
    }

    public String getVersion() {
        return version;
    }

    public void setChangelog(String changelog) {
        this.changelog = changelog;
    }

    public void setCommands(List<ListedCommands> commands) {
        this.commands = commands;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public void setDownload(String download) {
        this.download = download;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setGameVersions(List<String> game_versions) {
        this.game_versions = game_versions;
    }

    public void setHardDependencies(List<String> hard_dependencies) {
        this.hard_dependencies = hard_dependencies;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public void setPermissions(List<ListedPermissions> permissions) {
        this.permissions = permissions;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public void setSoftDependencies(List<String> soft_dependencies) {
        this.soft_dependencies = soft_dependencies;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}