package net.diamondmine.updater.types;

public class SearchPlugin {
    private String description;
    private String plugin_name;
    private String slug;

    public String getDescription() {
        return description;
    }

    public String getName() {
        return plugin_name;
    }

    public String getSlug() {
        return slug;
    }

}
