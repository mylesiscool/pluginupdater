package net.diamondmine.updater;

/**
 * Holds a global instance of {@link Updater}.
 * 
 * @author Jon la Cour
 */
public class UpdaterInstance {

    private static Updater updater;

    /**
     * Get an instance of PluginUpdater.
     * 
     * @return an instance of PluginUpdater
     */
    public static Updater getInstance() {
        if (updater == null) {
            throw new RuntimeException("Uh oh, PluginUpdater is not loaded as a plugin, but the API is being used!");
        }

        return updater;
    }

    /**
     * Get whether an instance is available.
     * 
     * @return true if there's an instance
     */
    public static boolean hasInstance() {
        return updater != null;
    }

    /**
     * Set the instance of PluginUpdater. This should only be called by
     * PluginUpdater itself.
     * 
     * @param updater
     *        the instance of PluginUpdater
     */
    public static void setInstance(Updater updater) {
        UpdaterInstance.updater = updater;
    }

}
