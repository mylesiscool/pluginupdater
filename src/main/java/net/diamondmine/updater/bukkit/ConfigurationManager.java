package net.diamondmine.updater.bukkit;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.sk89q.util.yaml.YAMLFormat;
import com.sk89q.util.yaml.YAMLProcessor;

public class ConfigurationManager {

    /**
     * Reference to the plugin.
     */
    private UpdaterPlugin plugin;

    /**
     * The global configuration for use when loading worlds
     */
    private YAMLProcessor config;

    /**
     * List of plugins that automatically update.
     */
    private List<String> automaticUpdatingPlugins = new ArrayList<String>();

    public int automaticUpdateDelay;

    /**
     * Construct the object.
     * 
     * @param plugin
     *        The plugin instance
     */
    public ConfigurationManager(UpdaterPlugin plugin) {
        this.plugin = plugin;
    }

    /**
     * Load the configuration.
     */
    public void load() {
        // Create the default configuration file
        plugin.createDefaultConfiguration(new File(plugin.getDataFolder(), "config.yml"), "config.yml");

        config = new YAMLProcessor(new File(plugin.getDataFolder(), "config.yml"), true, YAMLFormat.EXTENDED);
        try {
            config.load();
        } catch (IOException e) {
            plugin.getLogger().severe("Error reading configuration for global config: ");
            e.printStackTrace();
        }

        config.removeProperty("options");
        config.removeProperty("plugins");
        automaticUpdateDelay = config.getInt("automatic-updates.interval.hours", 2) * 72000;
        automaticUpdatingPlugins = config.getStringList("automatic-updates.plugins", new ArrayList<String>());

        if (!config.save()) {
            plugin.getLogger().severe("Error saving configuration!");
        }
    }

    /**
     * Unload the configuration.
     */
    public void unload() {
        config.setProperty("automatic-updates.plugins", automaticUpdatingPlugins);
        config.save();
    }

    /**
     * Enable automatic updating for a plugin.
     * 
     * @param player
     *        The plugin to enable automatic updating for
     */
    public void enableAutomaticUpdating(String plugin) {
        if (!automaticUpdatingPlugins.contains(plugin)) {
            automaticUpdatingPlugins.add(plugin);
        }
    }

    /**
     * Disable automatic updating for a plugin.
     * 
     * @param player
     *        The plugin to disable automatic updating for
     */
    public void disableAutomaticUpdating(String plugin) {
        if (automaticUpdatingPlugins.contains(plugin)) {
            automaticUpdatingPlugins.remove(plugin);
        }
    }

    /**
     * Get a list of plugins that have automatic updating set.
     * 
     * @return Plugins with automatic updating set
     */
    public List<String> getAutomaticUpdatingPlugins() {
        return automaticUpdatingPlugins;
    }

    /**
     * Check to see if a plugin has automatic updating.
     * 
     * @param player
     *        The plugin to check
     * @return Whether plugin has automatic updating
     */
    public boolean hasAutomaticUpdating(String plugin) {
        return automaticUpdatingPlugins.contains(plugin);
    }
}