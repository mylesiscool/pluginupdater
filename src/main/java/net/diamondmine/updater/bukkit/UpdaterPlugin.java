package net.diamondmine.updater.bukkit;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.jar.JarFile;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;

import net.diamondmine.updater.Updater;
import net.diamondmine.updater.UpdaterInstance;
import net.diamondmine.updater.bukkit.commands.GeneralCommands;
import net.diamondmine.updater.bukkit.commands.InstallCommand;
import net.diamondmine.updater.bukkit.commands.PluginUpdaterCommands;
import net.diamondmine.updater.bukkit.commands.SearchCommand;
import net.diamondmine.updater.bukkit.commands.UpdateCommand;
import net.diamondmine.updater.util.FatalConfigurationLoadingException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.sk89q.bukkit.util.CommandsManagerRegistration;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissionsException;
import com.sk89q.minecraft.util.commands.CommandUsageException;
import com.sk89q.minecraft.util.commands.CommandsManager;
import com.sk89q.minecraft.util.commands.MissingNestedCommandException;
import com.sk89q.minecraft.util.commands.SimpleInjector;
import com.sk89q.minecraft.util.commands.WrappedCommandException;
import com.sk89q.wepif.PermissionsResolverManager;

public class UpdaterPlugin extends JavaPlugin implements Updater {

    /**
     * Manager for commands.
     */
    private final CommandsManager<CommandSender> commands;

    /**
     * Handles all configuration.
     */
    private final ConfigurationManager configuration;

    /**
     * Used for automatic updating.
     */
    private UpdateManager updateManager;

    private static final Logger logger = Bukkit.getLogger();

    public UpdaterPlugin() {
        configuration = new ConfigurationManager(this);
        UpdaterInstance.setInstance(this);

        final UpdaterPlugin plugin = this;
        commands = new CommandsManager<CommandSender>() {

            @Override
            public boolean hasPermission(CommandSender player, String perm) {
                return plugin.hasPermission(player, perm);
            }
        };
    }

    /**
     * Plugin enabled.
     */
    @Override
    public final void onEnable() {
        commands.setInjector(new SimpleInjector(this));

        final CommandsManagerRegistration reg = new CommandsManagerRegistration(this, commands);
        reg.register(PluginUpdaterCommands.class);
        reg.register(GeneralCommands.class);
        reg.register(InstallCommand.class);
        reg.register(SearchCommand.class);
        reg.register(UpdateCommand.class);

        getDataFolder().mkdirs();

        PermissionsResolverManager.initialize(this);

        try {
            configuration.load();
        } catch (FatalConfigurationLoadingException e) {
            e.printStackTrace();
            getServer().shutdown();
        }

        updateManager = new UpdateManager(this);

        getServer().getScheduler().runTaskTimerAsynchronously(this, updateManager, configuration.automaticUpdateDelay,
                configuration.automaticUpdateDelay);

        (new UpdaterPluginListener(this)).registerEvents();

        new SessionManager();

        logger.info("[PluginUpdater] Version " + getDescription().getVersion() + " enabled.");
    }

    /**
     * Plugin disabled.
     */
    @Override
    public final void onDisable() {
        configuration.unload();
        this.getServer().getScheduler().cancelTasks(this);
    }

    /**
     * Handle a command.
     */
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        try {
            commands.execute(cmd.getName(), args, sender, sender);
        } catch (CommandPermissionsException e) {
            sender.sendMessage(ChatColor.RED + "You don't have permission.");
        } catch (MissingNestedCommandException e) {
            sender.sendMessage(ChatColor.RED + e.getUsage());
        } catch (CommandUsageException e) {
            sender.sendMessage(ChatColor.RED + e.getMessage());
            sender.sendMessage(ChatColor.RED + e.getUsage());
        } catch (WrappedCommandException e) {
            if (e.getCause() instanceof NumberFormatException) {
                sender.sendMessage(ChatColor.RED + "Number expected, string received instead.");
            } else {
                sender.sendMessage(ChatColor.RED + "An error has occurred. See console.");
                e.printStackTrace();
            }
        } catch (CommandException e) {
            sender.sendMessage(ChatColor.RED + e.getMessage());
        }

        return true;
    }

    /**
     * Get the global ConfigurationManager. USe this to access global
     * configuration values and per-world configuration values.
     * 
     * @return The global ConfigurationManager
     */
    public ConfigurationManager getGlobalStateManager() {
        return configuration;
    }

    /**
     * Create a default configuration file from the .jar.
     * 
     * @param actual
     *        The destination file
     * @param defaultName
     *        The name of the file inside the jar's defaults folder
     */
    public void createDefaultConfiguration(File actual, String defaultName) {

        // Make parent directories
        File parent = actual.getParentFile();
        if (!parent.exists()) {
            parent.mkdirs();
        }

        if (actual.exists()) {
            return;
        }

        InputStream input = null;
        try {
            JarFile file = new JarFile(getFile());
            ZipEntry copy = file.getEntry("defaults/" + defaultName);
            file.close();
            if (copy == null)
                throw new FileNotFoundException();
            input = file.getInputStream(copy);
        } catch (IOException e) {
            getLogger().severe("Unable to read default configuration: " + defaultName);
        }

        if (input != null) {
            FileOutputStream output = null;

            try {
                output = new FileOutputStream(actual);
                byte[] buf = new byte[8192];
                int length = 0;
                while ((length = input.read(buf)) > 0) {
                    output.write(buf, 0, length);
                }

                getLogger().info("Default configuration file written: " + actual.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (input != null) {
                        input.close();
                    }
                } catch (IOException ignore) {
                }

                try {
                    if (output != null) {
                        output.close();
                    }
                } catch (IOException ignore) {
                }
            }
        }
    }

    /**
     * Checks permissions.
     * 
     * @param sender
     *        The sender to check the permission on.
     * @param perm
     *        The permission to check the permission on.
     * @return whether {@code sender} has {@code perm}
     */
    public boolean hasPermission(CommandSender sender, String perm) {
        if (sender.isOp()) {
            if (!(sender instanceof Player)) {
                return true;
            }
        }

        // Invoke the permissions resolver
        if (sender instanceof Player) {
            Player player = (Player) sender;
            return PermissionsResolverManager.getInstance().hasPermission(player.getWorld().getName(),
                    player.getName(), perm);
        }

        return false;
    }

    /**
     * Checks permissions and throws an exception if permission is not met.
     * 
     * @param sender
     *        The sender to check the permission on.
     * @param perm
     *        The permission to check the permission on.
     * @throws CommandPermissionsException
     *         if {@code sender} doesn't have {@code perm}
     */
    public void checkPermission(CommandSender sender, String perm) throws CommandPermissionsException {
        if (!hasPermission(sender, perm)) {
            throw new CommandPermissionsException();
        }
    }

    public File getDataDirectory() {
        return getDataFolder();
    }

    public String getVersionNumber() {
        return this.getDescription().getVersion();
    }

}
