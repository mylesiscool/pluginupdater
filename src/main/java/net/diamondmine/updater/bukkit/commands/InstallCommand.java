package net.diamondmine.updater.bukkit.commands;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.diamondmine.updater.BukgetClient;
import net.diamondmine.updater.bukkit.PlayerSession;
import net.diamondmine.updater.bukkit.PluginReloader;
import net.diamondmine.updater.bukkit.SessionManager;
import net.diamondmine.updater.bukkit.UpdaterPlugin;
import net.diamondmine.updater.types.ListedPlugin;
import net.diamondmine.updater.types.ListedVersion;
import net.diamondmine.updater.types.SearchPlugin;
import net.diamondmine.updater.types.resource.JAR;
import net.diamondmine.updater.types.resource.Resource;
import net.diamondmine.updater.types.resource.Zip;
import net.diamondmine.updater.util.FatalSlugSearchException;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.libs.com.google.gson.Gson;
import org.bukkit.plugin.Plugin;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;

public class InstallCommand {

    @SuppressWarnings("unused")
    private final UpdaterPlugin plugin;
    private PlayerSession session;

    public InstallCommand(UpdaterPlugin plugin) {
        this.plugin = plugin;
    }

    @Command(aliases = { "install" }, usage = "[plugin]", desc = "Install a plugin.")
    @CommandPermissions({ "pluginupdater.update" })
    public void install(CommandContext args, CommandSender sender) throws CommandException {
        session = SessionManager.getSession(sender);
        
        String response = StringUtils.join(args.getSlice(1), '+');
        if (StringUtils.isNumeric(response)) {
            int choice = Integer.parseInt(response) - 1;
            if (session.getVersions() != null) {
                List<ListedVersion> versions = session.getVersions();
    
                if (versions != null && choice >= 0 && versions.get(choice) != null) {
                    ListedVersion version = versions.get(choice);
                    
                    List<String> hardDependencies = version.getHardDependencies();
                    List<String> softDependencies = version.getSoftDependencies();

                    Plugin[] loadedPlugins = Bukkit.getServer().getPluginManager().getPlugins();

                    for (Plugin loadedPlugin : loadedPlugins) {
                        String plugin = loadedPlugin.getName();

                        hardDependencies.remove(plugin);
                        softDependencies.remove(plugin);
                    }

                    if (hardDependencies.size() > 0) {
                        sender.sendMessage(ChatColor.DARK_RED + "Error: Missing hard dependencies. "
                                + StringUtils.join(hardDependencies, ", "));
                        return;
                    }

                    if (softDependencies.size() > 0) {
                        sender.sendMessage(ChatColor.RED + "Warning: Missing soft dependencies. "
                                + StringUtils.join(softDependencies, ", "));
                    }
    
                    sender.sendMessage(ChatColor.GOLD + "Installing " + session.getPluginName() + ", version "
                            + version.getVersion() + ".");
    
                    try {
                        URL url = new URL(version.getDownload());
    
                        if (url.getFile().toLowerCase().endsWith(".jar")) {
                            Resource resource = new JAR(session.getPluginName(), version.getFilename(), url);
                            resource.download();
                        } else if (url.getFile().toLowerCase().endsWith(".zip")) {
                            Resource resource = new Zip(session.getPluginName(), version.getFilename(), url);
                            resource.download();
                        } else {
                            sender.sendMessage("Unrecognized file format.");
                            return;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        sender.sendMessage(ChatColor.RED
                                + "A fatal error occurred when downloading that file. Report error in console.");
                        return;
                    }
    
                    PluginReloader reloader = new PluginReloader();
    
                    try {
                        reloader.loadPlugin(session.getPluginName());
                    } catch (Exception e) {
                        e.printStackTrace();
                        sender.sendMessage(ChatColor.RED + "There has been an error installing \""
                                + session.getPluginName() + "\". Report error in console.");
                        return;
                    }
    
                    sender.sendMessage(ChatColor.YELLOW + session.getPluginName() + " has been successfully installed.");
                } else {
                    sender.sendMessage(ChatColor.RED + "There is no version with that number.");
                }
    
                return;
            }
            
            if (session.getSearchResults() != null) {
                String slug = session.getSearchResults()[choice].getSlug();
                args = new CommandContext(args.getSlice(0)[0] + " " + slug);
                install(args, sender);
                return;
            }
        }

        String plugin = StringUtils.join(args.getSlice(1), ' ');

        Plugin[] loadedPlugins = Bukkit.getServer().getPluginManager().getPlugins();

        for (Plugin loadedPlugin : loadedPlugins) {
            if (loadedPlugin.getDescription().getName().equalsIgnoreCase(plugin)) {
                sender.sendMessage(ChatColor.RED + "That plugin is already installed.");
                return;
            }
        }

        sender.sendMessage(ChatColor.YELLOW + "Searching for plugin...");

        BukgetClient client = new BukgetClient();
        String assumedSlug = StringUtils.replace(plugin, " ", "-").toLowerCase();
        client.get("plugins/bukkit/" + assumedSlug);

        if (client.status == HttpStatus.SC_OK) {
            Gson gson = new Gson();

            ListedPlugin listedPlugin = gson.fromJson(new String(client.response), ListedPlugin.class);

            if (listedPlugin == null) {
                sender.sendMessage(ChatColor.YELLOW + "Searching for correct BukkitDev page...");

                String search = StringUtils.replaceChars(plugin, ' ', '+');
                String slug = null;

                try {
                    slug = slugSearch(search, false);
                } catch (FatalSlugSearchException e) {
                    sender.sendMessage(ChatColor.RED + "Unable to find correct page. Manual update required.");
                    return;
                }

                sender.sendMessage(ChatColor.YELLOW + "Correct page found! Continuing update process.");

                client.get("plugins/bukkit/" + slug);

                listedPlugin = gson.fromJson(new String(client.response), ListedPlugin.class);
            }

            sender.sendMessage(ChatColor.YELLOW + "Installing " + listedPlugin.getPluginName() + ".");
            
            if (!listedPlugin.getDescription().isEmpty()) {
                sender.sendMessage(ChatColor.GRAY + listedPlugin.getDescription());
            }
        
            sender.sendMessage(ChatColor.GOLD + "Please select a recent file to install (/install #):");

            List<ListedVersion> versions = listedPlugin.getVersions();
            List<ListedVersion> sessionVersions = new ArrayList<ListedVersion>();

            int versionCount = versions.size();

            if (versionCount > 5) {
                versionCount = 5;
            }

            for (int v = 0; v < versionCount; v++) {
                ListedVersion version = versions.get(v);

                sessionVersions.add(version);

                String number = ChatColor.GOLD + " " + String.valueOf(v + 1) + ". ";
                String date = DateFormatUtils.ISO_DATE_FORMAT.format(new Date(version.getDate() * 1000));

                sender.sendMessage(number + ChatColor.YELLOW + version.getVersion() + ChatColor.GRAY + " " + date
                        + ChatColor.ITALIC + " (" + version.getType() + " - "
                        + StringUtils.join(version.getGameVersions(), ", ") + ")");
            }

            if (!listedPlugin.getPluginName().isEmpty()) {
                plugin = listedPlugin.getPluginName();
            }

            session.setVersions(sessionVersions);
            session.setPluginName(plugin);
        } else {
            try {
                String looseSlug = slugSearch(plugin, true);
                sender.sendMessage(ChatColor.YELLOW + "Did you mean " + looseSlug + "?");
            } catch (FatalSlugSearchException e) {
                sender.sendMessage(ChatColor.RED + "An error has occurred: " + HttpStatus.getStatusText(client.status));
            }
        }
    }

    private String slugSearch(String search, boolean loose) throws FatalSlugSearchException {
        BukgetClient client = new BukgetClient();
        client.get("search/plugin_name/like/" + search);

        if (client.status == HttpStatus.SC_OK) {
            Gson gson = new Gson();

            SearchPlugin[] plugins = gson.fromJson(new String(client.response), SearchPlugin[].class);

            if (plugins.length == 1 || (loose && plugins.length > 0)) {
                SearchPlugin plugin = plugins[0];

                String slug = plugin.getSlug();

                return slug;
            } else {
                throw new FatalSlugSearchException();
            }
        } else {
            throw new FatalSlugSearchException();
        }
    }
}
