package net.diamondmine.updater.bukkit.commands;

import net.diamondmine.updater.bukkit.ConfigurationManager;
import net.diamondmine.updater.bukkit.UpdaterPlugin;

import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;

public class PluginUpdaterCommands {

    private final UpdaterPlugin plugin;

    public PluginUpdaterCommands(UpdaterPlugin plugin) {
        this.plugin = plugin;
    }

    @Command(aliases = { "autoupdate" }, usage = "[plugin] [true/false]", desc = "Configuration options.")
    @CommandPermissions({ "pluginupdater.admin" })
    public void autoupdate(CommandContext args, CommandSender sender) throws CommandException {
        if (args.argsLength() <= 1
                || !(args.getString(1).equalsIgnoreCase("false") || args.getString(1).equalsIgnoreCase("true"))) {
            sender.sendMessage(ChatColor.RED + "Invalid options. Enter plugin name followed by true or false.");
            return;
        }

        String pluginName = args.getString(0);

        if (pluginName.startsWith("\"")) {
            String[] pluginArgs = StringUtils.substringsBetween(pluginName, "\"", "\"");
            pluginName = pluginArgs[0];
        }

        boolean option = Boolean.parseBoolean(args.getString(1));

        ConfigurationManager config = this.plugin.getGlobalStateManager();

        if (option) {
            if (config.hasAutomaticUpdating(pluginName)) {
                sender.sendMessage(ChatColor.YELLOW + "Already in automatic updater schedule. Currently running every "
                        + (config.automaticUpdateDelay / 72000) + " hour(s).");
            } else {
                config.enableAutomaticUpdating(pluginName);
                sender.sendMessage(ChatColor.YELLOW + "Added to automatic updater schedule. Currently running every "
                        + (config.automaticUpdateDelay / 72000) + " hour(s).");
            }
        } else {
            config.disableAutomaticUpdating(pluginName);
            sender.sendMessage(ChatColor.YELLOW + "Removed from automatic updater schedule.");
        }
    }

    @Command(aliases = { "reload" }, desc = "Reload configuration", max = 0)
    @CommandPermissions({ "pluginupdater.admin" })
    public void reload(CommandContext args, CommandSender sender) throws CommandException {
        try {
            plugin.getGlobalStateManager().unload();
            plugin.getGlobalStateManager().load();
            sender.sendMessage("PluginUpdater configuration reloaded.");
        } catch (Throwable t) {
            sender.sendMessage("Error while reloading: " + t.getMessage());
        }
    }
}
