package net.diamondmine.updater.bukkit.commands;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.diamondmine.updater.BukgetClient;
import net.diamondmine.updater.bukkit.PlayerSession;
import net.diamondmine.updater.bukkit.PluginReloader;
import net.diamondmine.updater.bukkit.SessionManager;
import net.diamondmine.updater.bukkit.UpdaterPlugin;
import net.diamondmine.updater.types.ListedPlugin;
import net.diamondmine.updater.types.ListedVersion;
import net.diamondmine.updater.types.SearchPlugin;
import net.diamondmine.updater.types.resource.JAR;
import net.diamondmine.updater.types.resource.Resource;
import net.diamondmine.updater.types.resource.Zip;
import net.diamondmine.updater.util.Checksum;
import net.diamondmine.updater.util.FatalSlugSearchException;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.libs.com.google.gson.Gson;
import org.bukkit.plugin.Plugin;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;

public class UpdateCommand {

    @SuppressWarnings("unused")
    private final UpdaterPlugin plugin;
    private PlayerSession session;

    public UpdateCommand(UpdaterPlugin plugin) {
        this.plugin = plugin;
    }

    @Command(aliases = { "update" }, usage = "[plugin]", desc = "Updates a plugin.")
    @CommandPermissions({ "pluginupdater.update" })
    public void update(CommandContext args, CommandSender sender) throws CommandException {
        session = SessionManager.getSession(sender);

        if (args.argsLength() == 0) {
            sender.sendMessage(ChatColor.GOLD
                    + "Are you sure you want to check for updates on every plugin? (/update confirm)");
            session.setTime(new Date().getTime());
            return;
        }

        long delay = (new Date().getTime() - session.getTime()) / 1000 % 60;
        if (args.getString(0).equalsIgnoreCase("confirm") && (delay < 15)) {
            sender.sendMessage(ChatColor.YELLOW
                    + "Now checking for updates. If a plugin has available updates it will be listed.");
            scanPlugins(sender);
            return;
        }

        String update = StringUtils.join(args.getSlice(1), '+');
        if (StringUtils.isNumeric(update)) {
            int versionNumber = Integer.parseInt(update) - 1;

            List<ListedVersion> versions = session.getVersions();

            if (versions != null && versionNumber >= 0 && versions.get(versionNumber) != null) {
                ListedVersion version = versions.get(versionNumber);

                List<String> hardDependencies = version.getHardDependencies();
                List<String> softDependencies = version.getSoftDependencies();

                Plugin[] loadedPlugins = Bukkit.getServer().getPluginManager().getPlugins();

                for (Plugin loadedPlugin : loadedPlugins) {
                    String plugin = loadedPlugin.getName();

                    hardDependencies.remove(plugin);
                    softDependencies.remove(plugin);
                }

                if (hardDependencies.size() > 0) {
                    sender.sendMessage(ChatColor.DARK_RED + "Error: Missing hard dependencies. "
                            + StringUtils.join(hardDependencies, ", "));
                    return;
                }

                if (softDependencies.size() > 0) {
                    sender.sendMessage(ChatColor.RED + "Warning: Missing soft dependencies. "
                            + StringUtils.join(softDependencies, ", "));
                }

                sender.sendMessage(ChatColor.GOLD + "Updating " + session.getPluginName() + " to version "
                        + version.getVersion() + ".");

                PluginReloader reloader = new PluginReloader();

                try {
                    reloader.unloadPlugin(session.getPluginName());

                    URL url = new URL(version.getDownload());

                    if (url.getFile().toLowerCase().endsWith(".jar")) {
                        Resource resource = new JAR(session.getPluginName(), session.getFileName(), url);
                        resource.download();
                    } else if (url.getFile().toLowerCase().endsWith(".zip")) {
                        Resource resource = new Zip(session.getPluginName(), version.getFilename(), url);
                        resource.download();
                    } else {
                        sender.sendMessage("Unrecognized file format.");
                        return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    sender.sendMessage(ChatColor.RED
                            + "A fatal error occurred when downloading that file. Report error in console.");
                    return;
                }

                try {
                    reloader.loadPlugin(session.getPluginName());
                } catch (Exception e) {
                    e.printStackTrace();
                    sender.sendMessage(ChatColor.RED + "There has been an error updating \"" + session.getPluginName()
                            + "\". Report error in console.");
                    return;
                }

                sender.sendMessage(ChatColor.YELLOW + session.getPluginName() + " has been successfully updated.");
            } else {
                sender.sendMessage(ChatColor.RED + "There is no version with that number.");
            }

            return;
        }

        String plugin = StringUtils.join(args.getSlice(1), ' ');

        Plugin[] loadedPlugins = Bukkit.getServer().getPluginManager().getPlugins();
        String fileName = null;
        String slug = null;

        for (Plugin loadedPlugin : loadedPlugins) {
            if (loadedPlugin.getDescription().getName().equalsIgnoreCase(plugin)) {
                plugin = loadedPlugin.getDescription().getName();
                fileName = getFilename(loadedPlugin);
            }

            if (loadedPlugin.getName().toLowerCase().contains(plugin.toLowerCase())) {
                try {
                    slug = slugSearch(plugin);
                } catch (FatalSlugSearchException e) {
                    continue;
                }

                if (slug.equalsIgnoreCase(plugin)) {
                    sender.sendMessage(ChatColor.YELLOW + "Assuming " + plugin + " is "
                            + loadedPlugin.getDescription().getName() + "...");
                    plugin = loadedPlugin.getDescription().getName();
                    fileName = getFilename(loadedPlugin);
                }
            }
        }

        if (fileName == null) {
            sender.sendMessage(ChatColor.RED + "The plugin \"" + plugin + "\" is not installed. Check spelling.");
            return;
        }

        sender.sendMessage(ChatColor.YELLOW + "Checking for update...");

        BukgetClient client = new BukgetClient();

        String assumedSlug = StringUtils.replace(plugin, " ", "+").toLowerCase();
        if (slug != null) {
            assumedSlug = slug;
        }

        client.get("plugins/bukkit/" + assumedSlug);

        if (client.status == HttpStatus.SC_OK) {
            Gson gson = new Gson();

            ListedPlugin listedPlugin = gson.fromJson(new String(client.response), ListedPlugin.class);

            if (listedPlugin == null) {
                sender.sendMessage(ChatColor.YELLOW + "Searching for correct BukkitDev page...");

                String search = StringUtils.replaceChars(plugin, ' ', '+').toLowerCase();

                try {
                    slug = slugSearch(search, false);
                } catch (FatalSlugSearchException e) {
                    sender.sendMessage(ChatColor.RED + "Unable to find correct page. Manual update required.");
                    return;
                }

                sender.sendMessage(ChatColor.YELLOW + "Correct page found! Continuing update process.");

                client.get("plugins/bukkit/" + slug);

                listedPlugin = gson.fromJson(new String(client.response), ListedPlugin.class);
            }

            Plugin pl = Bukkit.getServer().getPluginManager().getPlugin(plugin);

            String currentVersion = pl.getDescription().getVersion();
            String latestVersion = listedPlugin.getVersions().get(0).getVersion();

            String currentMD5;
            try {
                currentMD5 = Checksum.getMD5Checksum("plugins" + File.separator + fileName);
            } catch (Exception e) {
                currentMD5 = null;
            }
            String latestMD5 = listedPlugin.getVersions().get(0).getMD5();

            if (currentVersion.equals(latestVersion) && currentMD5.equalsIgnoreCase(latestMD5)) {
                sender.sendMessage(ChatColor.YELLOW + listedPlugin.getPluginName() + " is up to date!");
                sender.sendMessage(ChatColor.GOLD + "Running version " + currentVersion
                        + ". MD5 hash verified with latest.");
            } else {
                String serverVersion = Bukkit.getBukkitVersion();
                
                sender.sendMessage(ChatColor.YELLOW + "You are running version " + currentVersion + " of "
                        + listedPlugin.getPluginName() + " on " + serverVersion + ".");
                sender.sendMessage(ChatColor.GOLD + "Please select a recent file to update to (/update #):");

                List<ListedVersion> versions = listedPlugin.getVersions();
                List<ListedVersion> sessionVersions = new ArrayList<ListedVersion>();

                int versionCount = versions.size();

                if (versionCount > 5) {
                    versionCount = 5;
                }

                for (int v = 0; v < versionCount; v++) {
                    ListedVersion version = versions.get(v);

                    sessionVersions.add(version);

                    String number = " " + ChatColor.GOLD + String.valueOf(v + 1) + ". ";
                    String date = DateFormatUtils.ISO_DATE_FORMAT.format(new Date(version.getDate() * 1000));
                    String ver = version.getVersion();
                    if (ver.equals(currentVersion)) {
                        ver = ChatColor.STRIKETHROUGH + ver;
                    }

                    sender.sendMessage(number + ChatColor.YELLOW + ver + ChatColor.GRAY + " " + date + ChatColor.ITALIC
                            + " (" + version.getType() + " - " + StringUtils.join(version.getGameVersions(), ", ")
                            + ")");
                }

                session.setVersions(sessionVersions);
                session.setPluginName(plugin);
                session.setFileName(fileName);
            }
        } else {
            String status = HttpStatus.getStatusText(client.status);

            if (status.equals("Not Found")) {
                try {
                    slug = slugSearch(plugin, true);
                    args = new CommandContext(args.getSlice(0)[0] + " " + slug);
                    update(args, sender);
                    return;
                } catch (FatalSlugSearchException e) {
                    ;
                }
            }

            sender.sendMessage(ChatColor.RED + "An error has occurred: " + status);
        }
    }

    private String slugSearch(String search, boolean loose) throws FatalSlugSearchException {
        BukgetClient client = new BukgetClient();
        client.get("search/plugin_name/like/" + search);

        if (client.status == HttpStatus.SC_OK) {
            Gson gson = new Gson();

            SearchPlugin[] plugins = gson.fromJson(new String(client.response), SearchPlugin[].class);

            if (plugins.length == 1 || (loose && plugins.length > 0)) {
                SearchPlugin plugin = plugins[0];

                String slug = plugin.getSlug();

                return slug;
            } else {
                throw new FatalSlugSearchException();
            }
        } else {
            throw new FatalSlugSearchException();
        }
    }

    private String slugSearch(String search) throws FatalSlugSearchException {
        BukgetClient client = new BukgetClient();
        client.get("search/slug/=/" + search);

        if (client.status == HttpStatus.SC_OK) {
            Gson gson = new Gson();

            SearchPlugin[] plugins = gson.fromJson(new String(client.response), SearchPlugin[].class);

            if (plugins.length == 1) {
                SearchPlugin plugin = plugins[0];

                String slug = plugin.getSlug();

                return slug;
            } else {
                throw new FatalSlugSearchException();
            }
        } else {
            throw new FatalSlugSearchException();
        }
    }

    private String getFilename(Plugin loadedPlugin) {
        String plugin = loadedPlugin.getDescription().getName();

        Class<? extends Plugin> pluginClass = Bukkit.getPluginManager().getPlugin(plugin).getClass();
        URL location = pluginClass.getProtectionDomain().getCodeSource().getLocation();

        return new File(location.getFile()).getName();
    }

    private void scanPlugins(CommandSender sender) {
        Plugin[] loadedPlugins = Bukkit.getServer().getPluginManager().getPlugins();

        int updates = 0;
        for (Plugin loadedPlugin : loadedPlugins) {
            String plugin = loadedPlugin.getName();

            BukgetClient client = new BukgetClient();

            String assumedSlug = StringUtils.replace(plugin, " ", "+").toLowerCase();

            client.get("plugins/bukkit/" + assumedSlug);

            if (client.status == HttpStatus.SC_OK) {
                Gson gson = new Gson();

                ListedPlugin listedPlugin = gson.fromJson(new String(client.response), ListedPlugin.class);

                if (listedPlugin == null) {
                    String search = StringUtils.replaceChars(plugin, ' ', '+').toLowerCase();
                    String slug;

                    try {
                        slug = slugSearch(search, false);
                    } catch (FatalSlugSearchException e) {
                        return;
                    }

                    client.get("plugins/bukkit/" + slug);

                    listedPlugin = gson.fromJson(new String(client.response), ListedPlugin.class);
                }

                Plugin pl = Bukkit.getServer().getPluginManager().getPlugin(plugin);

                String currentVersion = pl.getDescription().getVersion();
                String latestVersion = listedPlugin.getVersions().get(0).getVersion();

                String currentMD5;
                try {
                    String fileName = getFilename(pl);
                    currentMD5 = Checksum.getMD5Checksum("plugins" + File.separator + fileName);
                } catch (Exception e) {
                    currentMD5 = null;
                }
                String latestMD5 = listedPlugin.getVersions().get(0).getMD5();

                if (!currentVersion.equals(latestVersion) && !currentMD5.equalsIgnoreCase(latestMD5)) {
                    List<ListedVersion> versions = listedPlugin.getVersions();

                    updates++;
                    sender.sendMessage(ChatColor.GOLD + String.valueOf(updates) + ". " + ChatColor.YELLOW
                            + listedPlugin.getPluginName() + " (" + currentVersion + "):" + ChatColor.GRAY + " "
                            + versions.get(0).getVersion() + " Available");
                }
            }
        }

        sender.sendMessage(ChatColor.YELLOW + "All plugins have been checked. " + updates + " update(s) available.");
    }
}
