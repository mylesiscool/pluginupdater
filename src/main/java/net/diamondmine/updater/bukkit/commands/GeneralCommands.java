package net.diamondmine.updater.bukkit.commands;

import java.io.File;
import java.net.URL;

import net.diamondmine.updater.bukkit.PluginReloader;
import net.diamondmine.updater.bukkit.UpdaterPlugin;

import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;
import com.sk89q.minecraft.util.commands.NestedCommand;

public class GeneralCommands {

    @SuppressWarnings("unused")
    private final UpdaterPlugin plugin;

    public GeneralCommands(UpdaterPlugin plugin) {
        this.plugin = plugin;
    }

    @Command(aliases = { "pluginupdater", "pu" }, desc = "PluginUpdater admin commands")
    @NestedCommand({ PluginUpdaterCommands.class })
    public void pluginUpdater(CommandContext args, CommandSender sender) {
    }

    @Command(aliases = { "uninstall", "remove" }, usage = "[plugin ...]", desc = "Uninstalls plugins.")
    @CommandPermissions({ "pluginupdater.uninstall" })
    public void uninstall(CommandContext args, CommandSender sender) throws CommandException {
        if (System.getProperty("os.name").toLowerCase().startsWith("windows")) {
            sender.sendMessage(ChatColor.YELLOW + "This command is not available on Windows, due to file locking.");
        } else {
            PluginReloader reloader = new PluginReloader();

            String plugins[] = args.getSlice(1);

            for (int i = 0; i < plugins.length; i++) {
                String pluginName = plugins[i];

                if (pluginName.startsWith("\"") && plugins.length > 1) {
                    if (plugins[i + 1].endsWith("\"")) {
                        pluginName += " " + plugins[i + 1];
                        pluginName = pluginName.replace("\"", "");
                        i++;
                    }
                }

                for (Plugin plugin : Bukkit.getPluginManager().getPlugins()) {
                    if (plugin.getDescription().getName().equalsIgnoreCase(pluginName)) {
                        pluginName = plugin.getDescription().getName();
                    }
                }

                Class<? extends Plugin> pluginClass = Bukkit.getPluginManager().getPlugin(pluginName).getClass();
                URL location = pluginClass.getProtectionDomain().getCodeSource().getLocation();

                try {
                    reloader.unloadPlugin(pluginName);
                } catch (Exception e) {
                    e.printStackTrace();
                    sender.sendMessage(ChatColor.RED + "Error unloading \"" + pluginName
                            + "\". Report error in console.");
                    return;
                }

                String fileName = "plugins" + File.separator + new File(location.getFile()).getName();

                try {
                    FileUtils.moveFile(new File(fileName), new File(fileName + ".old"));
                } catch (Exception e) {
                    e.printStackTrace();
                    new File(fileName).deleteOnExit();
                    sender.sendMessage(ChatColor.RED + "Error soft-uninstalling \"" + pluginName
                            + "\". Report error in console.");
                    return;
                }

                sender.sendMessage(ChatColor.YELLOW + "Successfully unloaded and soft-uninstalled " + pluginName + ".");
            }
        }
    }
}
