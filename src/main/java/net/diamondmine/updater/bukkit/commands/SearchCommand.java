package net.diamondmine.updater.bukkit.commands;

import net.diamondmine.updater.BukgetClient;
import net.diamondmine.updater.bukkit.PlayerSession;
import net.diamondmine.updater.bukkit.SessionManager;
import net.diamondmine.updater.bukkit.UpdaterPlugin;
import net.diamondmine.updater.types.SearchPlugin;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.libs.com.google.gson.Gson;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;

public class SearchCommand {

    @SuppressWarnings("unused")
    private final UpdaterPlugin plugin;
    private PlayerSession session;

    public SearchCommand(UpdaterPlugin plugin) {
        this.plugin = plugin;
    }

    @Command(aliases = { "search" }, usage = "[search terms]", desc = "Search for plugins.")
    @CommandPermissions({ "pluginupdater.search" })
    public void search(CommandContext args, CommandSender sender) throws CommandException {
        session = SessionManager.getSession(sender);

        String search = StringUtils.join(args.getSlice(1), '+');
        int offset = 0;
        int page = 1;

        if (StringUtils.isNumeric(search)) {
            page = Integer.parseInt(search);
            if (((page - 1) * 5) >= session.getSearchResults().length) {
                sender.sendMessage(ChatColor.RED + "That page doesn't exist.");
                return;
            } else {
                offset = (page - 1) * 5;
            }
        } else if (search.length() <= 2) {
            sender.sendMessage(ChatColor.YELLOW + "Please enter 3 or more characters to search.");
            return;
        } else {
            String rawSearch = StringUtils.join(args.getSlice(1), ' ');
            sender.sendMessage(ChatColor.YELLOW + "Searching for \"" + rawSearch + "\".");
        }

        BukgetClient client = new BukgetClient();
        client.get("search/plugin_name/like/" + search);

        if (client.status == HttpStatus.SC_OK) {
            Gson gson = new Gson();

            SearchPlugin[] plugins;

            if (offset == 0) {
                plugins = gson.fromJson(new String(client.response), SearchPlugin[].class);
                session = SessionManager.resetSession(sender);
                session.setSearchResults(plugins);
            } else {
                plugins = session.getSearchResults();
            }

            int pages = ((Double) Math.ceil((double) plugins.length / 5.0)).intValue();
            sender.sendMessage(ChatColor.GOLD + "Search returned " + plugins.length + " results (/search #). Page " + page + "/"
                    + pages);

            if (plugins.length > 0) {
                int max = offset + 5;

                if (plugins.length < max) {
                    max = plugins.length;
                }

                for (int i = offset; i < max; i++) {
                    SearchPlugin plugin = plugins[i];

                    String description = plugin.getDescription();
                    String name = plugin.getName();
                    String slug = plugin.getSlug();

                    String number = ChatColor.GOLD + " " + String.valueOf(i + 1) + ". ";

                    if (description.isEmpty()) {
                        sender.sendMessage(number + ChatColor.YELLOW + name + ChatColor.GRAY + ChatColor.ITALIC + " ("
                                + slug + ")");
                    } else {
                        sender.sendMessage(number + ChatColor.YELLOW + name + ": " + ChatColor.GRAY + description
                                + ChatColor.ITALIC + " (" + slug + ")");
                    }
                }
            }
        } else {
            sender.sendMessage(ChatColor.RED + "An error has occurred: " + HttpStatus.getStatusText(client.status));
        }
    }
}
