package net.diamondmine.updater.bukkit;

import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;

public class UpdaterPluginListener implements Listener {

    private UpdaterPlugin plugin;

    /**
     * Construct the object;
     * 
     * @param plugin
     */
    public UpdaterPluginListener(UpdaterPlugin plugin) {
        this.plugin = plugin;
    }

    /**
     * Register events.
     */
    public void registerEvents() {
        final PluginManager pm = plugin.getServer().getPluginManager();
        pm.registerEvents(this, plugin);
    }
}
