package net.diamondmine.updater.bukkit;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.logging.Logger;

import net.diamondmine.updater.BukgetClient;
import net.diamondmine.updater.types.ListedPlugin;
import net.diamondmine.updater.types.ListedVersion;
import net.diamondmine.updater.types.SearchPlugin;
import net.diamondmine.updater.types.resource.JAR;
import net.diamondmine.updater.types.resource.Resource;
import net.diamondmine.updater.types.resource.Zip;
import net.diamondmine.updater.util.Checksum;
import net.diamondmine.updater.util.FatalSlugSearchException;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.libs.com.google.gson.Gson;
import org.bukkit.plugin.Plugin;

public class UpdateManager implements Runnable {

    private UpdaterPlugin plugin;
    private Logger logger;
    private int updates;
    private int failed;

    /**
     * Construct the object.
     * 
     * @param plugin
     *        The plugin instance
     */
    public UpdateManager(UpdaterPlugin plugin) {
        this.plugin = plugin;
        this.logger = plugin.getLogger();
    }

    /**
     * Run the task.
     */
    public void run() {
        ConfigurationManager config = plugin.getGlobalStateManager();
        List<String> plugins = config.getAutomaticUpdatingPlugins();

        logger.info("[PluginUpdater] Running automatic updater. " + plugins.size() + " plugins to check.");

        updates = 0;
        failed = 0;

        for (String pluginName : plugins) {
            BukgetClient client = new BukgetClient();
            String assumedSlug = StringUtils.replace(pluginName, " ", "-").toLowerCase();
            client.get("plugins/bukkit/" + assumedSlug);

            if (client.status == HttpStatus.SC_OK) {
                Gson gson = new Gson();

                ListedPlugin listedPlugin = gson.fromJson(new String(client.response), ListedPlugin.class);

                if (listedPlugin == null) {

                    String search = StringUtils.replaceChars(pluginName, ' ', '+');
                    String slug = null;

                    try {
                        slug = slugSearch(search);
                    } catch (FatalSlugSearchException e) {
                        logger.warning("[PluginUpdater] Invalid plugin in automatic update configuration, \""
                                + pluginName + "\". Check spelling.");
                        failed++;
                        return;
                    }

                    client.get("plugins/bukkit/" + slug);

                    listedPlugin = gson.fromJson(new String(client.response), ListedPlugin.class);
                }

                Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin(pluginName);

                String currentVersion = plugin.getDescription().getVersion();
                String latestVersion = listedPlugin.getVersions().get(0).getVersion();

                Plugin[] loadedPlugins = Bukkit.getServer().getPluginManager().getPlugins();
                String fileName = null;

                for (Plugin loadedPlugin : loadedPlugins) {
                    if (loadedPlugin.getDescription().getName().equalsIgnoreCase(pluginName)) {
                        pluginName = loadedPlugin.getDescription().getName();

                        Class<? extends Plugin> pluginClass = Bukkit.getPluginManager().getPlugin(pluginName)
                                .getClass();
                        URL location = pluginClass.getProtectionDomain().getCodeSource().getLocation();

                        fileName = new File(location.getFile()).getName();
                    }
                }

                String currentMD5;
                try {
                    currentMD5 = Checksum.getMD5Checksum("plugins" + File.separator + fileName);
                } catch (Exception e) {
                    currentMD5 = null;
                }
                String latestMD5 = listedPlugin.getVersions().get(0).getMD5();

                if (!currentVersion.equals(latestVersion) && !currentMD5.equalsIgnoreCase(latestMD5)) {
                    performUpdate(listedPlugin, pluginName, fileName, 0);
                    updates++;
                }
            }
        }

        if (failed == 0) {
            logger.info("[PluginUpdater] Automatic updater complete. " + updates + "/" + plugins.size()
                    + " plugins needed an update.");
        } else {
            logger.info("[PluginUpdater] Automatic updater complete. " + updates + "/" + plugins.size()
                    + " plugins needed an update. " + failed + " updates failed.");
        }
    }

    private void performUpdate(ListedPlugin listedPlugin, String pluginName, String fileName, int backdate) {
        List<ListedVersion> versions = listedPlugin.getVersions();

        if (versions.size() > 0 && versions.get(backdate) != null) {
            ListedVersion version = versions.get(backdate);

            PluginReloader reloader = new PluginReloader();

            try {
                reloader.unloadPlugin(pluginName);

                URL url = new URL(version.getDownload());

                if (url.getFile().toLowerCase().endsWith(".jar")) {
                    Resource resource = new JAR(pluginName, fileName, url);
                    resource.download();
                } else if (url.getFile().toLowerCase().endsWith(".zip")) {
                    Resource resource = new Zip(pluginName, version.getFilename(), url);
                    resource.download();
                } else {
                    logger.warning("[PluginUpdater] Latest version of \"" + pluginName
                            + "\" had an invalid file format. Will not update.");
                    failed++;
                    return;
                }
            } catch (Exception e) {
                e.printStackTrace();
                logger.warning("[PluginUpdater] Error downloading update for \"" + pluginName
                        + "\". Report error above.");
                failed++;
                return;
            }

            try {
                reloader.loadPlugin(pluginName);
            } catch (Exception e) {
                e.printStackTrace();
                logger.warning("[PluginUpdater] Error downloading update for \"" + pluginName
                        + "\", restart may be required. Report error above to plugin author.");
                failed++;
                return;
            }

            logger.info("[PluginUpdater] Automatically updated \"" + pluginName + "\" to version "
                    + version.getVersion() + ".");
        } else {
            if (backdate == 0) {
                logger.warning("[PluginUpdater] No files available for \"" + pluginName + "\" for update!");
            } else {
                logger.severe("[PluginUpdater] All attempted backdates failed for \"" + pluginName + "\"!");
                failed++;
            }
        }
    }

    private String slugSearch(String search) throws FatalSlugSearchException {
        BukgetClient client = new BukgetClient();
        client.get("search/plugin_name/like/" + search);

        if (client.status == HttpStatus.SC_OK) {
            Gson gson = new Gson();

            SearchPlugin[] plugins = gson.fromJson(new String(client.response), SearchPlugin[].class);

            if (plugins.length == 1) {
                SearchPlugin plugin = plugins[0];

                String slug = plugin.getSlug();

                return slug;
            } else {
                throw new FatalSlugSearchException();
            }
        } else {
            throw new FatalSlugSearchException();
        }
    }

}
