package net.diamondmine.updater.bukkit;

import java.io.File;
import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.SimplePluginManager;

public class PluginReloader {

    /**
     * Unloads a plugin by name.
     * 
     * @param pluginName
     *        The name of the plugin to unload.
     * @param errorHandling
     *        True if we should send errors to command sender.
     * @param sender
     *        The command sender for error handling.
     * @return boolean depending on success
     * @throws Exception
     *         Thrown when a plugin cannot be unloaded.
     */
    @SuppressWarnings("unchecked")
    public boolean unloadPlugin(final String pluginName) throws Exception {
        PluginManager manager = Bukkit.getServer().getPluginManager();
        SimplePluginManager spmanager = (SimplePluginManager) manager;

        if (spmanager != null) {
            Field pluginsField = spmanager.getClass().getDeclaredField("plugins");
            pluginsField.setAccessible(true);
            List<Plugin> plugins = (List<Plugin>) pluginsField.get(spmanager);

            Field lookupNamesField = spmanager.getClass().getDeclaredField("lookupNames");
            lookupNamesField.setAccessible(true);
            Map<String, Plugin> lookupNames = (Map<String, Plugin>) lookupNamesField.get(spmanager);

            Field commandMapField = spmanager.getClass().getDeclaredField("commandMap");
            commandMapField.setAccessible(true);
            SimpleCommandMap commandMap = (SimpleCommandMap) commandMapField.get(spmanager);

            Field knownCommandsField = null;
            Map<String, Command> knownCommands = null;

            if (commandMap != null) {
                knownCommandsField = commandMap.getClass().getDeclaredField("knownCommands");
                knownCommandsField.setAccessible(true);
                knownCommands = (Map<String, Command>) knownCommandsField.get(commandMap);
            }

            for (Plugin plugin : manager.getPlugins()) {
                if (plugin.getDescription().getName().equalsIgnoreCase(pluginName)) {
                    manager.disablePlugin(plugin);

                    if (plugins != null && plugins.contains(plugin)) {
                        plugins.remove(plugin);
                    }

                    if (lookupNames != null && lookupNames.containsKey(pluginName)) {
                        lookupNames.remove(pluginName);
                    }

                    if (commandMap != null) {
                        for (Iterator<Map.Entry<String, Command>> it = knownCommands.entrySet().iterator(); it
                                .hasNext();) {
                            Map.Entry<String, Command> entry = it.next();

                            if (entry.getValue() instanceof PluginCommand) {
                                PluginCommand command = (PluginCommand) entry.getValue();

                                if (command.getPlugin() == plugin) {
                                    command.unregister(commandMap);
                                    it.remove();
                                }
                            }
                        }
                    }
                }
            }
        }

        return true;
    }

    /**
     * Loads a plugin by name.
     * 
     * @param pluginName
     *        The name of the plugin to load.
     * @param errorHandling
     *        True if we should send errors to command sender.
     * @param sender
     *        The command sender for error handling.
     * @return boolean depending on success
     * @since 1.0.0
     */
    public boolean loadPlugin(final String pluginName) {
        try {
            PluginManager manager = Bukkit.getServer().getPluginManager();
            Plugin plugin = manager.loadPlugin(new File("plugins", pluginName + ".jar"));

            if (plugin == null) {
                return false;
            }

            plugin.onLoad();
            manager.enablePlugin(plugin);
        } catch (Exception e) {
            return false;
        }

        return true;
    }

}
