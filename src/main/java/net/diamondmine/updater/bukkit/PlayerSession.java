package net.diamondmine.updater.bukkit;

import java.util.List;

import net.diamondmine.updater.types.ListedVersion;
import net.diamondmine.updater.types.SearchPlugin;

import org.bukkit.command.CommandSender;

/**
 * Stores data specific to each player on the server. This class is persistent
 * over play quit and rejoins, but not over server reboots
 * 
 * @author oliverw92
 */
public class PlayerSession {

    private long time = 0;
    private String fileName = null;
    private String pluginName = null;
    private SearchPlugin[] searchResults = null;
    private CommandSender sender;
    private List<ListedVersion> versions = null;

    public PlayerSession(CommandSender sender) {
        this.setSender(sender);
    }
    
    public long getTime() {
        return time;
    }

    public String getFileName() {
        return fileName;
    }

    public String getPluginName() {
        return pluginName;
    }

    public SearchPlugin[] getSearchResults() {
        return searchResults;
    }

    public CommandSender getSender() {
        return sender;
    }

    public List<ListedVersion> getVersions() {
        return versions;
    }
    
    public void setTime(long time) {
        this.time = time;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setPluginName(String pluginName) {
        this.pluginName = pluginName;
    }

    public void setSearchResults(SearchPlugin[] searchResults) {
        this.searchResults = searchResults;
    }

    public void setSender(CommandSender sender) {
        this.sender = sender;
    }

    public void setVersions(List<ListedVersion> versions) {
        this.versions = versions;
    }

}
