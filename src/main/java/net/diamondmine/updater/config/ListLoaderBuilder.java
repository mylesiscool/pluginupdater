package net.diamondmine.updater.config;

import java.util.List;

class ListLoaderBuilder implements Loader<List<Object>>, Builder<List<Object>> {

    public Object write(List<Object> value) {
        return value;
    }

    @SuppressWarnings("unchecked")
    public List<Object> read(Object value) {
        if (value instanceof List) {
            return (List<Object>) value;
        } else {
            return null;
        }
    }

}
