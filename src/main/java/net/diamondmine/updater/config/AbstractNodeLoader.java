package net.diamondmine.updater.config;

import java.util.Map;

public abstract class AbstractNodeLoader<V> implements Loader<V> {

    public V read(Object value) {
        if (value != null && value instanceof ConfigurationNode) {
            return read((ConfigurationNode) value);
        } else if (value != null && value instanceof Map) {
            return read(new ConfigurationNode(value));
        } else {
            return null;
        }
    }

    public abstract V read(ConfigurationNode node);

}
