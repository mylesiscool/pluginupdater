package net.diamondmine.updater.config;

public abstract class AbstractLoader<V> implements Loader<V> {

    public V read(Object value) {
        return read(new ConfigurationValue(value));
    }

    public abstract V read(ConfigurationValue value);

}
