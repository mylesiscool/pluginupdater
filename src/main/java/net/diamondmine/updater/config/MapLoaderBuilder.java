package net.diamondmine.updater.config;

import java.util.Map;

class MapLoaderBuilder implements Loader<Map<Object, Object>>, Builder<Map<Object, Object>> {

    public Object write(Map<Object, Object> value) {
        return value;
    }

    @SuppressWarnings("unchecked")
    public Map<Object, Object> read(Object value) {
        if (value instanceof Map) {
            return (Map<Object, Object>) value;
        } else {
            return null;
        }
    }

}
