package net.diamondmine.updater.config.types;

import net.diamondmine.updater.config.Builder;
import net.diamondmine.updater.config.Loader;

public class LowercaseStringLoaderBuilder implements Loader<String>, Builder<String> {

    public Object write(String value) {
        return String.valueOf(value);
    }

    public String read(Object value) {
        return String.valueOf(value).toLowerCase();
    }

}
