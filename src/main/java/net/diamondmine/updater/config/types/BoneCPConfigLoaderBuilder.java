package net.diamondmine.updater.config.types;

import net.diamondmine.updater.config.AbstractNodeLoader;
import net.diamondmine.updater.config.Builder;
import net.diamondmine.updater.config.ConfigurationNode;
import net.diamondmine.updater.config.LoaderBuilderException;

import com.jolbox.bonecp.BoneCPConfig;

public class BoneCPConfigLoaderBuilder extends AbstractNodeLoader<BoneCPConfig> implements Builder<BoneCPConfig> {

    public Object write(BoneCPConfig config) throws LoaderBuilderException {
        ConfigurationNode node = new ConfigurationNode();
        node.set("dsn", config.getJdbcUrl());
        node.set("username", config.getUsername());
        node.set("password", config.getPassword());

        return node;
    }

    @Override
    public BoneCPConfig read(ConfigurationNode node) {
        BoneCPConfig config = new BoneCPConfig();
        config.setJdbcUrl(node.getString("dsn", ""));
        config.setUsername(node.getString("username", "root"));
        config.setPassword(node.getString("password", ""));

        return config;
    }

}
