package net.diamondmine.updater.config.types;

import net.diamondmine.updater.config.Builder;
import net.diamondmine.updater.config.ConfigurationNode;
import net.diamondmine.updater.config.Loader;
import net.diamondmine.updater.util.MapBuilder.ObjectMapBuilder;

import org.bukkit.util.BlockVector;
import org.bukkit.util.Vector;

public class VectorLoaderBuilder implements Loader<Vector>, Builder<Vector> {

    private final boolean asBlock;

    public VectorLoaderBuilder() {
        this(false);
    }

    public VectorLoaderBuilder(boolean asBlock) {
        this.asBlock = asBlock;
    }

    public Object write(Vector value) {
        return new ObjectMapBuilder().put("x", value.getX()).put("y", value.getY()).put("z", value.getZ()).map();
    }

    public Vector read(Object value) {
        ConfigurationNode node = new ConfigurationNode(value);
        Double x = node.getDouble("x");
        Double y = node.getDouble("y");
        Double z = node.getDouble("z");

        if (x == null || y == null || z == null) {
            return null;
        }

        return asBlock ? new BlockVector(x, y, z) : new Vector(x, y, z);
    }

}
