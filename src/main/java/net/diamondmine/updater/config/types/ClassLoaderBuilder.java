package net.diamondmine.updater.config.types;

import java.util.logging.Logger;

import net.diamondmine.updater.config.Builder;
import net.diamondmine.updater.config.Loader;

public class ClassLoaderBuilder implements Loader<Class<?>>, Builder<Class<?>> {

    private Logger logger = Logger.getLogger(ClassLoaderBuilder.class.getCanonicalName());

    public Class<?> read(Object value) {
        String stringValue = String.valueOf(value);
        try {
            return Class.forName(stringValue);
        } catch (ClassNotFoundException e) {
            logger.warning("ClassResolver: Could not find class " + stringValue);
            return null;
        }
    }

    public Object write(Class<?> value) {
        return value.getCanonicalName();
    }

}