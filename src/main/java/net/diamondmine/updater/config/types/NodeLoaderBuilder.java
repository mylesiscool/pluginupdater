package net.diamondmine.updater.config.types;

import java.util.Map;

import net.diamondmine.updater.config.Builder;
import net.diamondmine.updater.config.ConfigurationNode;
import net.diamondmine.updater.config.Loader;

public class NodeLoaderBuilder implements Loader<ConfigurationNode>, Builder<ConfigurationNode> {

    public Object write(ConfigurationNode value) {
        return value;
    }

    public ConfigurationNode read(Object value) {
        if (value instanceof Map) {
            return new ConfigurationNode(value);
        } else {
            return null;
        }
    }

}
