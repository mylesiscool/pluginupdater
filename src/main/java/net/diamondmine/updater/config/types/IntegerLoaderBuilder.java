package net.diamondmine.updater.config.types;

import net.diamondmine.updater.config.Builder;
import net.diamondmine.updater.config.Loader;

public class IntegerLoaderBuilder implements Loader<Integer>, Builder<Integer> {

    public Object write(Integer value) {
        return value;
    }

    public Integer read(Object value) {
        return valueOf(value);
    }

    public static Integer valueOf(Object o) {
        if (o == null) {
            return null;
        } else if (o instanceof Byte) {
            return (int) (Byte) o;
        } else if (o instanceof Integer) {
            return (Integer) o;
        } else if (o instanceof Double) {
            return (int) (double) (Double) o;
        } else if (o instanceof Float) {
            return (int) (float) (Float) o;
        } else if (o instanceof Long) {
            return (int) (long) (Long) o;
        } else {
            return null;
        }
    }

}
