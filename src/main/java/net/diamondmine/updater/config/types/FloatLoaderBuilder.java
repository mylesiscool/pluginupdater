package net.diamondmine.updater.config.types;

import net.diamondmine.updater.config.Builder;
import net.diamondmine.updater.config.Loader;

public class FloatLoaderBuilder implements Loader<Float>, Builder<Float> {

    public Object write(Float value) {
        return value;
    }

    public Float read(Object value) {
        return valueOf(value);
    }

    public static Float valueOf(Object o) {
        if (o == null) {
            return null;
        } else if (o instanceof Float) {
            return (Float) o;
        } else if (o instanceof Double) {
            return (Float) o;
        } else if (o instanceof Byte) {
            return (float) (Byte) o;
        } else if (o instanceof Integer) {
            return (float) (Integer) o;
        } else if (o instanceof Long) {
            return (float) (Long) o;
        } else {
            return null;
        }
    }

}
