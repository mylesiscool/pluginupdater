package net.diamondmine.updater.config.types;

import java.util.Map;

import net.diamondmine.updater.config.Builder;
import net.diamondmine.updater.config.Loader;

class MapLoaderBuilder implements Loader<Map<Object, Object>>, Builder<Map<Object, Object>> {

    public Object write(Map<Object, Object> value) {
        return value;
    }

    @SuppressWarnings("unchecked")
    public Map<Object, Object> read(Object value) {
        if (value instanceof Map) {
            return (Map<Object, Object>) value;
        } else {
            return null;
        }
    }

}
