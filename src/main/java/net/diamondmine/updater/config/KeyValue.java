package net.diamondmine.updater.config;

import java.util.Map;

public class KeyValue<K, V> implements Map.Entry<K, V> {

    private K key;
    private V value;

    public KeyValue(K key, V entry) {
        this.key = key;
        this.value = entry;
    }

    public K getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }

    public V setValue(V value) {
        V old = value;
        this.value = value;
        return old;
    }

}
