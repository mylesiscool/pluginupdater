package net.diamondmine.updater.config;

import java.util.Map.Entry;

public class DummyKeyValueBuilder<K, V> implements KeyValueLoader<K, V>, KeyValueBuilder<K, V> {

    private final KeyValueLoader<K, V> loader;

    public DummyKeyValueBuilder(KeyValueLoader<K, V> loader) {
        this.loader = loader;
    }

    public Entry<Object, Object> write(K key, V value) {
        return null;
    }

    public Entry<K, V> read(Object key, Object value) {
        return loader.read(key, value);
    }

}
