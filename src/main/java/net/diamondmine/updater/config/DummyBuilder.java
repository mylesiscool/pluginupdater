package net.diamondmine.updater.config;

public class DummyBuilder<V> implements Loader<V>, Builder<V> {

    private final Loader<V> loader;

    public DummyBuilder(Loader<V> loader) {
        this.loader = loader;
    }

    public Object write(V value) {
        return null;
    }

    public V read(Object value) {
        return loader.read(value);
    }

}
