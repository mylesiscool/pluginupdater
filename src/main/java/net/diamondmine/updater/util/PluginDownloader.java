package net.diamondmine.updater.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class PluginDownloader {
    /**
     * Downloads the resource.
     * 
     * @throws IOException
     *         Error downloading file.
     * @return boolean based on success of download.
     */
    public final static boolean download(URL url, String fileName) throws IOException {
        if (url == null) {
            return false;
        }

        ReadableByteChannel channel = Channels.newChannel(url.openStream());
        FileOutputStream fos = new FileOutputStream("plugins" + File.separator + fileName);
        fos.getChannel().transferFrom(channel, 0, 1 << 24);
        fos.close();
        
        return true;
    }
}
