package net.diamondmine.updater.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.security.MessageDigest;

public class Checksum {
    /**
     * Creates a checksum.
     * 
     * @param filename
     *        Filename or path to file.
     * @return MD5 string.
     * @throws Exception
     *         Invalid file.
     */
    private static byte[] createChecksum(final String filename) throws Exception {
        InputStream fis = new FileInputStream(filename);

        byte[] buffer = new byte[1024];
        MessageDigest complete = MessageDigest.getInstance("MD5");
        int numRead;

        do {
            numRead = fis.read(buffer);
            if (numRead > 0) {
                complete.update(buffer, 0, numRead);
            }
        } while (numRead != -1);

        fis.close();
        return complete.digest();
    }

    /**
     * Gets a file's MD5 checksum.
     * 
     * @param filename
     *        Filename or path to file.
     * @return MD5 string.
     * @throws Exception
     *         Invalid file.
     */
    public static String getMD5Checksum(final String filename) throws Exception {
        byte[] b = createChecksum(filename);
        String result = "";

        for (byte element : b) {
            result += Integer.toString((element & 0xff) + 0x100, 16).substring(1);
        }
        return result;
    }
}
